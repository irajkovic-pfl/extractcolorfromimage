from flask import Flask, render_template, request, redirect, flash, url_for
from werkzeug.utils import secure_filename
import numpy as np
import collections
from PIL import Image, UnidentifiedImageError
import os


# Convert rbg color to hex
def rgb_to_hex(rgb):
    return '#{:02x}{:02x}{:02x}'.format(rgb[0], rgb[1], rgb[2])


# App
app = Flask(__name__)
app.config['SECRET_KEY'] = '2!j@56czUkTQ53'
app.config['UPLOAD_FOLDER'] = 'static/uploads/'


# Main route
@app.route('/', methods=['GET', 'POST'])
def index():

    # Form is sent
    if request.method == 'POST':

        # Check if the post request has the file part
        if 'file' not in request.files:
            flash('No file in form.')
            return redirect(url_for('index'))
        else:
            file = request.files['file']

        # Check if the file part is empty
        if file.filename == '':
            flash('No file selected.')
            return redirect(url_for('index'))
        else:
            # Save file to uploads folder
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            # Check if image file can be opened
            try:
                image = Image.open(app.config['UPLOAD_FOLDER'] + filename)
            except UnidentifiedImageError:
                flash('Invalid image file.')
                os.remove(app.config['UPLOAD_FOLDER'] + filename)
                return redirect(url_for('index'))
            else:
                # Load image as array and prepare for processing
                image_array = np.array(image)
                height, width, channels = image_array.shape

                # Maps all colors in image into a list
                colors_list = list()
                for h in range(height):
                    for w in range(width):
                        color = rgb_to_hex(image_array[h, w])
                        colors_list.append(color)

                # Format most_common list to view model
                total_color_count = len(colors_list)
                most_common = list()
                for color in collections.Counter(colors_list).most_common()[:int(request.form['number'])]:
                    most_common.append((color[0], round(color[1] / total_color_count * 100, 6)))

                return render_template('index.html', most_common=most_common, file="static/uploads/" + filename)
    else:
        return render_template('index.html')


# Run
if __name__ == '__main__':
    app.run(debug=True)
